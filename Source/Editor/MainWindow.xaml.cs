﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using FSEditor.FSData;
using System.IO;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Editor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public Int32 SnapShift
        {
            get
            {
                return _SnapShift;
            }

            set
            {
                if (_SnapShift == value)
                    return;

                _SnapShift = value;
                NotifyPropertyChanged();
            }
        }
        private Int32 _SnapShift;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var border = (Border)VisualTreeHelper.GetChild(PART_Squares, 0);

            // Get scrollviewer
            ScrollViewer scrollViewer = border.Child as ScrollViewer;
            if (scrollViewer != null)
            {
                // center the Scroll Viewer...
                double cx = scrollViewer.ScrollableWidth / 2.0;
                scrollViewer.ScrollToHorizontalOffset(cx);

                double cy = scrollViewer.ScrollableHeight / 2.0;
                scrollViewer.ScrollToVerticalOffset(cy);
            }

            Open_Click(null, null);
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            var saveFileDialog = new SaveFileDialog();
            if (saveFileDialog.ShowDialog() != true)
                return;

            using (var stream = saveFileDialog.OpenFile())
            {
                MiscUtil.IO.EndianBinaryWriter binWriter = new MiscUtil.IO.EndianBinaryWriter(MiscUtil.Conversion.EndianBitConverter.Big, stream);
                ((BoardFile)this.DataContext).WriteToStream(binWriter);
            }
        }

        private void Open_Click(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() != true)
            {
                return;
            }

            using (var stream = openFileDialog.OpenFile())
            {
                MiscUtil.IO.EndianBinaryReader binReader = new MiscUtil.IO.EndianBinaryReader(MiscUtil.Conversion.EndianBitConverter.Big, stream);
                var Board = BoardFile.LoadFromStream(binReader);
                this.DataContext = Board;
            }
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            var board = ((BoardFile)this.DataContext);

            if (board == null)
                return;

            var newSquare = new SquareData()
            {
                Id = (byte)board.BoardData.Squares.Count,
                Position = new Position(0, 0),
                Waypoint1 = new WaypointData()
                {
                    EntryId = 255,
                    Destination1 = 255,
                    Destination2 = 255,
                    Destination3 = 255,
                },
                Waypoint2 = new WaypointData()
                {
                    EntryId = 255,
                    Destination1 = 255,
                    Destination2 = 255,
                    Destination3 = 255,
                },
                Waypoint3 = new WaypointData()
                {
                    EntryId = 255,
                    Destination1 = 255,
                    Destination2 = 255,
                    Destination3 = 255,
                },
                Waypoint4 = new WaypointData()
                {
                    EntryId = 255,
                    Destination1 = 255,
                    Destination2 = 255,
                    Destination3 = 255,
                },
            };

            newSquare.Waypoints = new WaypointData[] { newSquare.Waypoint1, newSquare.Waypoint2, newSquare.Waypoint3, newSquare.Waypoint4, };

            board.BoardData.Squares.Add(newSquare);
        }

        private void Remove_Click(object sender, RoutedEventArgs e)
        {
            var board = ((BoardFile)this.DataContext);
            var selected = (SquareData)PART_Squares.SelectedItem;

            if (board == null || selected == null)
                return;

            board.BoardData.Squares.Remove(selected);

            for (byte i = 0; i < board.BoardData.Squares.Count; ++i)
            {
                var square = board.BoardData.Squares[i];
                square.Id = i;

                for (int j = 0; j < 4; ++j)
                {
                    if (square.Waypoints[j].EntryId == selected.Id)
                        square.Waypoints[j].EntryId = 255;
                    else if (square.Waypoints[j].EntryId > selected.Id && square.Waypoints[j].EntryId != 255)
                        square.Waypoints[j].EntryId--;

                    if (square.Waypoints[j].Destination1 == selected.Id)
                        square.Waypoints[j].Destination1 = 255;
                    else if (square.Waypoints[j].Destination1 > selected.Id && square.Waypoints[j].Destination1 != 255)
                        square.Waypoints[j].Destination1--;

                    if (square.Waypoints[j].Destination2 == selected.Id)
                        square.Waypoints[j].Destination2 = 255;
                    else if (square.Waypoints[j].Destination2 > selected.Id && square.Waypoints[j].Destination2 != 255)
                        square.Waypoints[j].Destination2--;

                    if (square.Waypoints[j].Destination3 == selected.Id)
                        square.Waypoints[j].Destination3 = 255;
                    else if (square.Waypoints[j].Destination3 > selected.Id && square.Waypoints[j].Destination3 != 255)
                        square.Waypoints[j].Destination3--;
                }
            }
        }
        private void Snap16_Click(object sender, RoutedEventArgs e)
        {
            var selected = (SquareData)PART_Squares.SelectedItem;

            if (selected == null)
                return;

            selected.Position.X = RoundBy(selected.Position.X, 16);
            selected.Position.Y = RoundBy(selected.Position.Y, 16);
        }

        private void Snap32_Click(object sender, RoutedEventArgs e)
        {
            var selected = (SquareData)PART_Squares.SelectedItem;

            if (selected == null)
                return;

            selected.Position.X = RoundBy(selected.Position.X, 32);
            selected.Position.Y = RoundBy(selected.Position.Y, 32);
        }

        private void Snap64_Click(object sender, RoutedEventArgs e)
        {
            var selected = (SquareData)PART_Squares.SelectedItem;

            if (selected == null)
                return;

            selected.Position.X = RoundBy(selected.Position.X, 64);
            selected.Position.Y = RoundBy(selected.Position.Y, 64);
        }

        private void SnapAll0_Click(object sender, RoutedEventArgs e)
        {
            SnapShift = 0;
        }

        private void SnapAll8_Click(object sender, RoutedEventArgs e)
        {
            SnapShift = 3;
        }

        private void SnapAll16_Click(object sender, RoutedEventArgs e)
        {
            SnapShift = 4;
        }

        private void SnapAll32_Click(object sender, RoutedEventArgs e)
        {
            SnapShift = 5;
        }

        private void SnapAll64_Click(object sender, RoutedEventArgs e)
        {
            SnapShift = 6;
        }

        private Int16 RoundBy(Int16 x, Int16 y)
        {
            if (Math.Abs(x % y) > (short)(y / 2))
                return (short)(((short)(x / y) + 1) * y);
            else return (short)((short)(x / y) * y);
        }

        private void Verify_Click(object sender, RoutedEventArgs e)
        {
            var board = ((BoardFile)this.DataContext);

            if (board == null)
                return;

            StringBuilder sb = new StringBuilder();

            if (board.BoardInfo.MaxDiceRoll > 8)
                sb.AppendLine("MaxDiceRoll should be less than or equal to 8");

            if (board.BoardData.Squares.Count(t => t.SquareType == SquareType.Bank) != 1)
                sb.AppendLine("Board must have exactly 1 Bank");

            if (board.BoardData.Squares.Count > 0 && board.BoardData.Squares[0].SquareType != SquareType.Bank)
                sb.AppendLine("Square 0 should be a Bank");

            if (board.BoardData.Squares.Count < 3)
                sb.AppendLine("Board must have at least 3 spaces or else players will be unable to move");

            // Verify each district has between 3-6 properties
            foreach (var group in board.BoardData.Squares
                .Where(t => t.SquareType == SquareType.VacantPlot || t.SquareType == SquareType.Property)
                .GroupBy(t => t.DistrictDestinationId))
            {
                if (group.Count() < 3 || group.Count() > 6)
                    sb.AppendFormat("District {0} has {1} properties/vacant plots which may cause unpredicitable behavior. Between 3 and 6 properties/vacant plots recommended.\n", group.Key, group.Count());
            }

            foreach (var square in board.BoardData.Squares)
            {
                for (int i = 0; i < 4; ++i)
                {
                    //Entry
                    if (square.Waypoints[i].EntryId >= board.BoardData.Squares.Count)
                    {
                        if (square.Waypoints[i].EntryId != 255)
                            sb.AppendFormat("Square {0}, Waypoint {1}, EntryId references square {2} which does not exist\n", square.Id, i + 1, square.Waypoints[i].EntryId);
                    }
                    else
                    {
                        if (square.SquareType != SquareType.OneWayAlleySquare)
                        {
                            var other = board.BoardData.Squares[square.Waypoints[i].EntryId];
                            if (Math.Abs(square.Position.X - other.Position.X) > 64
                                || Math.Abs(square.Position.Y - other.Position.Y) > 64)
                            {
                                sb.AppendFormat("Square {0}, Waypoint {1}, EntryId references square {2} which is not touching\n", square.Id, i + 1, square.Waypoints[i].EntryId);
                            }
                        }
                    }

                    //Destination1
                    if (square.Waypoints[i].Destination1 >= board.BoardData.Squares.Count)
                    {
                        if (square.Waypoints[i].Destination1 != 255)
                            sb.AppendFormat("Square {0}, Waypoint {1}, Destination1 references square {2} which does not exist\n", square.Id, i + 1, square.Waypoints[i].Destination1);
                    }
                    else
                    {
                        if (square.SquareType != SquareType.OneWayAlleyDoorA
                            && square.SquareType != SquareType.OneWayAlleyDoorB
                            && square.SquareType != SquareType.OneWayAlleyDoorC
                            && square.SquareType != SquareType.OneWayAlleyDoorD)
                        {
                            var other = board.BoardData.Squares[square.Waypoints[i].Destination1];
                            if (Math.Abs(square.Position.X - other.Position.X) > 64
                                || Math.Abs(square.Position.Y - other.Position.Y) > 64)
                            {
                                sb.AppendFormat("Square {0}, Waypoint {1}, Destiniation1 references square {2} which is not touching\n", square.Id, i + 1, square.Waypoints[i].Destination1);
                            }
                        }
                    }

                    //Destination2
                    if (square.Waypoints[i].Destination2 >= board.BoardData.Squares.Count)
                    {
                        if (square.Waypoints[i].Destination2 != 255)
                            sb.AppendFormat("Square {0}, Waypoint {1}, Destination2 references square {2} which does not exist\n", square.Id, i + 1, square.Waypoints[i].Destination2);
                    }
                    else
                    {
                        if (square.SquareType != SquareType.OneWayAlleyDoorA
                               && square.SquareType != SquareType.OneWayAlleyDoorB
                               && square.SquareType != SquareType.OneWayAlleyDoorC
                               && square.SquareType != SquareType.OneWayAlleyDoorD)
                        {
                            var other = board.BoardData.Squares[square.Waypoints[i].Destination2];
                            if (Math.Abs(square.Position.X - other.Position.X) > 64
                                || Math.Abs(square.Position.Y - other.Position.Y) > 64)
                            {
                                sb.AppendFormat("Square {0}, Waypoint {1}, Destination2 references square {2} which is not touching\n", square.Id, i + 1, square.Waypoints[i].Destination2);
                            }
                        }
                    }

                    //Destination3
                    if (square.Waypoints[i].Destination3 >= board.BoardData.Squares.Count)
                    {
                        if (square.Waypoints[i].Destination3 != 255)
                            sb.AppendFormat("Square {0}, Waypoint {1}, Destination3 references square {2} which does not exist\n", square.Id, i + 1, square.Waypoints[i].Destination3);
                    }
                    else
                    {
                        if (square.SquareType != SquareType.OneWayAlleyDoorA
                            && square.SquareType != SquareType.OneWayAlleyDoorB
                            && square.SquareType != SquareType.OneWayAlleyDoorC
                            && square.SquareType != SquareType.OneWayAlleyDoorD)
                        {
                            var other = board.BoardData.Squares[square.Waypoints[i].Destination3];
                            if (Math.Abs(square.Position.X - other.Position.X) > 64
                                || Math.Abs(square.Position.Y - other.Position.Y) > 64)
                            {
                                sb.AppendFormat("Square {0}, Waypoint {1}, Destination3 references square {2} which is not touching\n", square.Id, i + 1, square.Waypoints[i].Destination3);
                            }
                        }
                    }
                }
            }

            string result = sb.ToString();
            if (String.IsNullOrWhiteSpace(result))
                MessageBox.Show("Board Good", "Board Verification", MessageBoxButton.OK, MessageBoxImage.Information);
            else
                MessageBox.Show(sb.ToString(), "Board Verification", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void Help_Click(object sender, RoutedEventArgs e)
        {
            var helpWin = new HelpWindow();
            helpWin.ShowDialog();
        }

        private void Autopath_Click(object sender, RoutedEventArgs e)
        {
            var board = ((BoardFile)this.DataContext);

            if (board == null)
                return;

            foreach (var square in board.BoardData.Squares)
            {
                var touchingSquares = board.BoardData.Squares.Where(s => s != square && Math.Abs(square.Position.X - s.Position.X) <= 64
                            && Math.Abs(square.Position.Y - s.Position.Y) <= 64).ToArray();

                if (touchingSquares.Length > 0)
                {
                    square.Waypoint1.EntryId = touchingSquares[0].Id;

                    if (touchingSquares.Length > 1)
                        square.Waypoint1.Destination1 = touchingSquares[1].Id;
                    else square.Waypoint1.Destination1 = 255;

                    if (touchingSquares.Length > 2)
                        square.Waypoint1.Destination2 = touchingSquares[2].Id;
                    else square.Waypoint1.Destination2 = 255;

                    if (touchingSquares.Length > 3)
                        square.Waypoint1.Destination3 = touchingSquares[3].Id;
                    else square.Waypoint1.Destination3 = 255;
                }
                else
                {
                    square.Waypoint1.EntryId = 255;
                    square.Waypoint1.Destination1 = 255;
                    square.Waypoint1.Destination2 = 255;
                    square.Waypoint1.Destination3 = 255;
                }

                if (touchingSquares.Length > 1)
                {
                    square.Waypoint2.EntryId = touchingSquares[1].Id;
                    square.Waypoint2.Destination1 = touchingSquares[0].Id;

                    if (touchingSquares.Length > 2)
                        square.Waypoint2.Destination2 = touchingSquares[2].Id;
                    else square.Waypoint2.Destination2 = 255;

                    if (touchingSquares.Length > 3)
                        square.Waypoint2.Destination3 = touchingSquares[3].Id;
                    else square.Waypoint2.Destination3 = 255;
                }
                else
                {
                    square.Waypoint2.EntryId = 255;
                    square.Waypoint2.Destination1 = 255;
                    square.Waypoint2.Destination2 = 255;
                    square.Waypoint2.Destination3 = 255;
                }

                if (touchingSquares.Length > 2)
                {
                    square.Waypoint3.EntryId = touchingSquares[2].Id;
                    square.Waypoint3.Destination1 = touchingSquares[0].Id;
                    square.Waypoint3.Destination2 = touchingSquares[1].Id;

                    if (touchingSquares.Length > 3)
                        square.Waypoint3.Destination3 = touchingSquares[3].Id;
                    else square.Waypoint3.Destination3 = 255;
                }
                else
                {
                    square.Waypoint3.EntryId = 255;
                    square.Waypoint3.Destination1 = 255;
                    square.Waypoint3.Destination2 = 255;
                    square.Waypoint3.Destination3 = 255;
                }

                if (touchingSquares.Length > 3)
                {
                    square.Waypoint4.EntryId = touchingSquares[3].Id;
                    square.Waypoint4.Destination1 = touchingSquares[0].Id;
                    square.Waypoint4.Destination2 = touchingSquares[1].Id;
                    square.Waypoint4.Destination3 = touchingSquares[2].Id;
                }
                else
                {
                    square.Waypoint4.EntryId = 255;
                    square.Waypoint4.Destination1 = 255;
                    square.Waypoint4.Destination2 = 255;
                    square.Waypoint4.Destination3 = 255;
                }
            }

            MessageBox.Show("Autopathed entire map");
        }

        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property. 
        // The CallerMemberName attribute that is applied to the optional propertyName 
        // parameter causes the property name of the caller to be substituted as an argument. 
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}

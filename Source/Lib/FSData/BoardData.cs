﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MiscUtil.IO;
using System.IO;
using System.Drawing;
using System.Collections.ObjectModel;

namespace FSEditor.FSData {
	public class BoardData : Header {
		#region Fields & Properties

        public const uint Size = 8;

		// ----------------------------------------------------------------------------------------------------

        public UInt32 Unknown { get; set; }

		/// <summary>
		/// Gets or sets the number of squares in this board.
		/// </summary>
		public UInt16 SquareCount { get; set; }

        public UInt16 Unknown2 { get; set; }

        /// <summary>
		/// Gets or sets the squares in this board.
		/// </summary>
		public ObservableCollection<SquareData> Squares { get; set; }
		// ----------------------------------------------------------------------------------------------------
		#endregion

		#region Initialization
		// ----------------------------------------------------------------------------------------------------
		/// <summary>
		/// Default Constructor
		/// </summary>
		public BoardData() : base("I4PL") {
		}
		// ----------------------------------------------------------------------------------------------------
		#endregion

		#region Loading & Writing Methods
		// ----------------------------------------------------------------------------------------------------
		/// <summary>
		/// Loads a board from a stream.
		/// </summary>
		/// <param name="stream">The stream to read from.</param>
		/// <returns>A new BoardData object representing the contents of a FortuneStreet board.</returns>
		public static BoardData LoadFromStream(EndianBinaryReader stream) {
			BoardData board = new BoardData();

			// Verify Header & Read FileSize
			board.ReadMagicNumberAndHeaderSize(stream);

			// Read Square Count
            board.Unknown = stream.ReadUInt32();
            board.SquareCount = stream.ReadUInt16();
            board.Unknown2 = stream.ReadUInt16();

			// Loading Square Data
            board.Squares = new ObservableCollection<SquareData>();
			for (Byte i = 0; i < board.SquareCount; i++) {
				board.Squares.Add(SquareData.LoadFromStream(stream, i));
			}

			return board;
		}

        public void WriteToStream(EndianBinaryWriter stream)
        {
            HeaderSize = (UInt32)(Size + (Squares.Count * SquareData.Size));

			// Verify Header & Read FileSize
            WriteMagicNumberAndHeaderSize(stream);

			// Read Square Count
            stream.Write(Unknown);
            stream.Write((UInt16)Squares.Count);
            stream.Write(Unknown2);

			// Loading Square Data
            for (Byte i = 0; i < Squares.Count; i++)
            {
                Squares[i].WriteToStream(stream);
			}
        }
		// ----------------------------------------------------------------------------------------------------
		#endregion
	}
}

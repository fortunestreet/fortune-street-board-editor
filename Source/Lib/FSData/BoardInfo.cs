﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MiscUtil.IO;
using System.IO;

namespace FSEditor.FSData {
	public class BoardInfo : Header {

        public new const uint Size = 24;

		#region Fields & Properties
        /// <summary>
        /// Unknown always seems to be 0
        /// </summary>
        public UInt64 Unknown { get; set; }

		/// <summary>
		/// Gets or sets the initial amount of cash all players start with.
		/// </summary>
		public UInt16 InitialCash { get; set; }

		/// <summary>
		/// Gets or sets the target amount of money.
		/// </summary>
		public UInt16 TargetAmount { get; set; }

		/// <summary>
		/// Gets or sets the base salary value.
		/// Salary is calculated: Salary = Base * (Level * Increment)
		/// </summary>
		public UInt16 BaseSalary { get; set; }

		/// <summary>
		/// Gets or sets the salary increment value.
		/// Salary is calculated: Salary = Base * (Level * Increment)
		/// </summary>
		public UInt16 SalaryIncrement { get; set; }

		/// <summary>
		/// Gets or sets the size of the dice.
		/// </summary>
		public UInt16 MaxDiceRoll { get; set; }

        /// <summary>
        /// Unknown always seems to be 0
        /// </summary>
        public UInt32 Unknown2 { get; set; }

        /// <summary>
        /// Unknown always seems to be 0
        /// </summary>
        public UInt16 Unknown3 { get; set; }
		// ----------------------------------------------------------------------------------------------------
		#endregion

		#region Initialization
		// ----------------------------------------------------------------------------------------------------
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public BoardInfo()  : base("I4DT") {
			this.MaxDiceRoll = 6;
		} 
		// ----------------------------------------------------------------------------------------------------
		#endregion

		#region Loading & Writing Methods
		// ----------------------------------------------------------------------------------------------------
		/// <summary>
		/// Loads board information from a stream.
		/// </summary>
		/// <param name="stream">The stream to read from.</param>
		/// <returns>A new BoardInfo object holding information about the board.</returns>
		public static BoardInfo LoadFromStream(EndianBinaryReader stream) {
			BoardInfo boardInfo = new BoardInfo();

			// Verify Header & Read FileSize
			boardInfo.ReadMagicNumberAndHeaderSize(stream);

            // Read Unknown
            boardInfo.Unknown = stream.ReadUInt64();

			// Read Initial Cash
			boardInfo.InitialCash = stream.ReadUInt16();

			// Read Target Amount
			boardInfo.TargetAmount = stream.ReadUInt16();

			// Read Base Salary Valuve
			boardInfo.BaseSalary = stream.ReadUInt16();

			// Read Salary Increment Value
			boardInfo.SalaryIncrement = stream.ReadUInt16();

            // Read MaxDiceRoll
            boardInfo.MaxDiceRoll = stream.ReadUInt16();

            boardInfo.Unknown2 = stream.ReadUInt32();

            boardInfo.Unknown3 = stream.ReadUInt16();

			return boardInfo;
		}

        public void WriteToStream(EndianBinaryWriter stream)
        {
            HeaderSize = Size;

            // Verify Header & Read FileSize
            WriteMagicNumberAndHeaderSize(stream);

            // Write Unknown
            stream.Write(Unknown);

            // Read Initial Cash
            stream.Write(InitialCash);

            // Read Target Amount
            stream.Write(TargetAmount);

            // Read Base Salary Valuve
            stream.Write(BaseSalary);

            // Read Salary Increment Value
            stream.Write(SalaryIncrement);

            stream.Write(MaxDiceRoll);

            stream.Write(Unknown2);

            stream.Write(Unknown3);
        }
		// ----------------------------------------------------------------------------------------------------
		#endregion
	}
}
